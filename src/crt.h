//GlitchD1's Tiny Computer
//Cartridge Access

#define CODE_MAX_LENGTH		4096
#define TITLE_MAX_LENGTH	16
#define CART_SIZE           8192
#define CART_WIDTH          64
#define USER_SIZE			480
#define USER_CHECKSIZE		15

int CRT_Init(char *filename);

char *CRT_GetCode();
char *CRT_GetTitle();
char *CRT_GetMusic();
char **CRT_GetSpriteMap();
char *CRT_GetMap();

char CRT_User_GetChar(int id);
int CRT_User_GetInt(int id);
const char *CRT_User_GetString(int id, int length);

int CRT_User_PutChar(int id, char v);
int CRT_User_PutInt(int id, int v);
int CRT_User_PutString(int id, int l, const char *v);

int CRT_User_Check();
int CRT_User_Sync();

void CRT_SaveCart(const char *filename);

void CRT_Quit();
