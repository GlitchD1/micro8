//GlitchD1's Tiny Computer
//Code Processing Unit Header

int CPU_Init();

int CPU_Parse(char *code);

int CPU_Loop();

void CPU_Quit();
