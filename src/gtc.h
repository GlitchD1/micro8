//GlitchD1's Tiny Computer
//Global Variables

#include <stdbool.h>

#define SCREEN_SIZE		64
#define FRAME_RATE		33

static inline char hw_set( char hw, char hw_o, char hw_n)
{
	hw_n = hw_n & 0x0F;
	if(hw % 2)
	{
		hw_o = hw_o & 0xF0;
	}
	else
	{
		hw_o = hw_o & 0x0F;
		hw_n = hw_n << 4;
	}
	return hw_o | hw_n;
}

static inline char hw_get( int hw, char w)
{
	if(hw % 2)
	{
		return w & 0x0F;
	}
	else
	{
		return (w & 0xF0) >> 4;
	}
}
