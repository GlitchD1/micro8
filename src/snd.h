//GlitchD1's Tiny Computer
//Sound System Header

#define NOTE_TYPE_SQUARE	0
#define NOTE_TYPE_TRIANGLE	1
#define NOTE_TYPE_SAWTOOTH	2
#define NOTE_TYPE_NOISE		3
#define SHEET_NOTES			1024
#define NOTE_PERIOD_MAX		64
#define CHANNEL_NUM			4

int SND_Init();

char SND_Note_Create(char type, char period);

void SND_Channel_Volume(char channel, char volume);

void SND_Channel_PlayFromBank(char channel, short start, short end, short beat);

void SND_Channel_PlaySingle(char channel, char note, short time);

void SND_Sheet_SetNote(short pos, char note);

void SND_Sheet_SetAll(char notes[SHEET_NOTES]);

void SND_Update();

void SND_Quit();
