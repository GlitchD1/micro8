//GlitchD1's Tiny Computer
//Cartridge Access

#define DEBUG_CODE "CRT"

//default cartridge
#include "gtc.h"
#include "debug.h"
#include "crt.h"
#include "snd.h"
#include "vid.h"
#include "txt.h"
#include "img_crt.h"

#include "ext_lodepng.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef union _cartridge {
	struct {
	char title[TITLE_MAX_LENGTH+1];
	char code[CODE_MAX_LENGTH];
	char music[SHEET_NOTES];
	char sprite[SPRITE_MAX][SPRITE_SIZE];
	char map[MAP_SIZE];
	char user[USER_SIZE];
	char check[USER_CHECKSIZE];
	};
	char block[CART_SIZE];
} _cartridge;

_cartridge cart;

char *cart_filename;

char d_cart[] = "CART-TITLE\n"
"Built-in Demo\n"
"CART-CODE\n"
"set sc 1\n"
"set x 28\n"
"set y 14\n"
"set dx 1\n"
"set dy 1\n"
"vol 0 128\n"
"func loop {} {\n"
"cls [expr $sc / 64]\n"
"map [expr -$x * 16 / 56] 0 0 0 10 8\n"
"println '  - GTC Demo -'\n"
"note 0 $sc 1\n"
"inc x $dx\n"
"inc y $dy\n"
"if [expr $x > 56 || $x < 0] {set dx [expr -$dx]\n"
"note 2 31 1}\n"
"if [expr $y > 48 || $y < 8] {set dy [expr -$dy]\n"
"note 2 16 1}\n"
"draw $x $y 16\n"
"printat $x [expr $y + 10] [lastkey]\n"
"inc sc\n"
"color [expr $sc /8]\n"
"circ 0 56 8 8\n"
"fcirc 10 56 8 8\n"
"rect 20 56 8 8\n"
"frect 30 56 8 8\n"
"hline 56 60 64\n"
"vline 40 56 64\n"
"line 56 56 64 64\n"
"line 32 32 $x $y\n"
"line 32 32 0 0\n"
"line 32 32 16 0\n"
"line 32 32 32 0\n"
"line 32 32 48 0\n"
"line 32 32 64 0\n"
"line 32 32 64 16\n"
"line 32 32 64 32\n"
"line 32 32 64 48\n"
"line 32 32 64 64\n"
"line 32 32 48 64\n"
"line 32 32 32 64\n"
"line 32 32 16 64\n"
"line 32 32 0 64\n"
"line 32 32 0 48\n"
"line 32 32 0 32\n"
"line 32 32 0 16\n"
"if [key space] {set sc 0}\n"
"if [key e] {error 'I broke everything. Yay!'}\n"
"if [key s] {play 1 0 31 10}\n"
"if [key r] {static}\n"
"}\n"
"CART-MUSIC\n"
"36 36 38 40 40 38 36 34 32 32 34 36 36 36 34 34\n"
"36 36 38 40 40 38 36 34 32 32 34 36 34 34 32 32\n"
"CART-MAP\n"
"11111111110000000000000000000000\n"
"00000333330000000000000000000000\n"
"00000333330000000000000000000000\n"
"00000333330000000000000000000000\n"
"00000333330000000000000000000000\n"
"00000333330000000000000000000000\n"
"00000333330000000000000000000000\n"
"22222222220000000000000000000000\n"
"CART-TILES\n"
"3434343443434343343434344343434334343434434343433434343443434343\n"
"00000000000000000000000000000000000000000000000000000000FFFFFFFF\n"
"FFFFFFFF00000000000000000000000000000000000000000000000000000000\n"
"8A8A8A8AA8A8A8A88A8A8A8AA8A8A8A88A8A8A8AA8A8A8A88A8A8A8AA8A8A8A8\n"
"CART-SPRITES\n"
"000FF00000FFFF000FF77FF0FF7007FFFF7007FF0FF77FF000FFFF00000FF000\n"
"CART-ICON\n"
"01234567F0123456EF012345DEF01234CDEF0123BCDEF012ABCDEF019ABCDEF0\n"
"89ABCDEF789ABCDE6789ABCD56789ABC456789AB3456789A2345678912345678\n"
"89ABCDEF789ABCDE6789ABCD56789ABC456789AB3456789A2345678912345678\n"
"01234567F0123456EF012345DEF01234CDEF0123BCDEF012ABCDEF019ABCDEF0\n"
"CART-DONE\n";

#define MUC_TITLE	0
#define MUC_CODE	1
#define MUC_MUSIC	2
#define MUC_MAP		3
#define MUC_TILES	4
#define MUC_SPRITES	5
#define MUC_ICON	6
#define MUC_DONE	7
#define MUC_MAX		8

int _CRT_SpriteCompile(char *ptr, int start, int end)
{
    int current = start;
    char *token = strtok(ptr, "\n");
    while(token)
    {
        if (current == end)
        {
        	trace("Reached End of Sprite Block.");
        	return false;
        }
        if (strlen(token) != SPRITE_SIZE*2)
        {
            trace("Invalid Sprite Size on Sprite %d", current);
        }
        // Determine bytes for each sprite pixel pair.
        for(int c = 0; c < SPRITE_SIZE*2; c+=2)
        {
            char a = token[c];
            char b = token[c+1];
            //First Pixel per Byte
            if (a >= '0' && a <= '9')
                a = a - '0';
            else if (a >= 'A' && a <= 'F')
                a = a - 'A' + 10;
            else
            {
                trace("Invalid Character in Sprite %d", current);
                return false;
            }
            //Second Pixel per Byte
            if (b >= '0' && b <= '9')
                b = b - '0';
            else if (b >= 'A' && b <= 'F')
                b = b - 'A' + 10;
            else
            {
                trace("Invalid Character in Sprite %d", current);
                return false;
            }
            //Assembled Pixels
            cart.sprite[current][c/2] = ((a & 0x0f) << 4) | b & 0x0f;
        }
        //Get next token
        ++current;
        token = strtok(NULL, " \n");
    }
    return true;
}

int _CRT_Compile(const char *data)
{
    //Static Attack the block set.
    for (int i = 0; i < CART_SIZE;++i)
    {
        cart.block[i] = rand();
    }
    int length = 0;
	int current = 0;
	char hwb[2] = {0, 0};
    int hwc = 0;
	char *token;
	char *block = malloc(strlen(data)+1);
	strcpy(block, data);
	//Find Tags
	trace("Finding Tags.");
	char *tags[MUC_MAX];
	tags[MUC_TITLE] = strstr(block, "CART-TITLE");
	tags[MUC_CODE] = strstr(block, "CART-CODE");
	tags[MUC_MUSIC] = strstr(block, "CART-MUSIC");
	tags[MUC_MAP] = strstr(block, "CART-MAP");
	tags[MUC_TILES] = strstr(block, "CART-TILES");
	tags[MUC_SPRITES] = strstr(block, "CART-SPRITES");
	tags[MUC_ICON] = strstr(block, "CART-ICON");
	tags[MUC_DONE] = strstr(block, "CART-DONE");
	//Check and Scrub Tags
	trace("Scrubbing Tags.");
	for(int i = 0; i < MUC_MAX; i++)
	{
		if(tags[i])
		{
			char *ptr = strchr(tags[i],'\n')+1;
			*(tags[i]) = 0;
			tags[i] = ptr;
		}
		else
		{
			trace("MUC File is Missing a Tag.");
			return false;
		}
	}
    //Compile Title
    trace("Compiling Title.");
    length = strcspn(tags[MUC_TITLE], "\n");
    if (length > TITLE_MAX_LENGTH)
    {
        trace("Title is too long. Title can't be longer than TITLE_MAX_LENGTH characters");
        return false;
    }
    strncpy(cart.title, tags[MUC_TITLE], length);
    cart.title[length] = '\0';
    //Compile Code
    trace("Compiling Code.");
    length = strlen(tags[MUC_CODE]);
    if (length > CODE_MAX_LENGTH)
    {
        trace("Code is too long. Code can't be longer than CODE_MAX_LENGTH characters");
        return false;
    }
    strncpy(cart.code, tags[MUC_CODE], length);
    cart.code[length] = '\0';
    trace("Complied Code Length: %d",length);
    //Compile Music
    trace("Compiling Music Sheet.");
    current = 0;
    token = strtok(tags[MUC_MUSIC], " \n");
    while(token)
    {
        if (current == SHEET_NOTES)
        {
        	trace("Too many Music Notes. You can't have more than SHEET_NOTES notes");
        	return false;
        }
        //TODO - Do a thing.
        char note = atoi(token);
        cart.music[current] = note;
        ++current;
        token = strtok(NULL, " \n");
    }
    //Compile Map
    trace("Compiling Map Data.");
    current = 0;
    length = strlen(tags[MUC_MAP]);
    hwc = 0;
	for(int c = 0; c < length; ++c)
    {
		char a = tags[MUC_MAP][c];
		//First Pixel per Byte
		if (a >= '0' && a <= '9')
			a = a - '0';
		else if (a >= 'A' && a <= 'F')
			a = a - 'A' + 10;
        else
            continue;

        if (current == MAP_SIZE)
        {
            trace("Too many Map tiles, You can't have more than MAP_SIZE tiles in the map.");
            return false;
        }

        hwb[hwc] = a;
        ++hwc;

		//Assembled Pixels
		if (hwc == 2)
		{
            cart.map[current] = ((hwb[0] & 0x0f) << 4) | hwb[1] & 0x0f;
            hwc = 0;
            ++current;
		}
	}
	if (hwc == 1) //Clean up the odd end problem
    {
        cart.map[current] =  (hwb[0] & 0x0f) << 4;
    }
    //Compile Tiles
    trace("Compiling Tiles.");
    if (!_CRT_SpriteCompile(tags[MUC_TILES], 0, 16))
    {
        trace("Unable to Compile Tiles.");
        return false;
    }
    //Compile Sprites
    trace("Compiling Sprites.");
    if (!_CRT_SpriteCompile(tags[MUC_SPRITES], 16, 60))
    {
        trace("Unable to Compile Sprites.");
        return false;
    }
    //Compile Icon
    trace("Compiling Icon.");
    if (!_CRT_SpriteCompile(tags[MUC_ICON], 60, 64))
    {
        trace("Unable to Compile Icon.");
        return false;
    }
	//Clean up.
	trace("Cleaning up.");
	//free(block); - Apparently this breaks stuff, I don't know why...

	//We finished Compiling. Everything is awesome.
	trace("All done compiling.");
	return true;
}

bool _filecheck(const char *filename, const char *ext)
{
	return !strcmp(&filename[strlen(filename)-strlen(ext)],ext);
}

int CRT_Init(char *filename)
{
	if (!filename)
	{
	    cart_filename = "./d_cart.muc";
		return _CRT_Compile(d_cart);
	}
	else if (_filecheck(filename, ".muc"))
	{
	    cart_filename = filename;
		//Load MUC File, then compile it.
		char *source = NULL;
		FILE *fp = fopen(filename, "r");
		if (fp == NULL)
		{
			print("Couldn't open MUC File");
			return false;
		}

		// Get the size of the file.
		if (fseek(fp, 0, SEEK_END) != 0)
		{
			print("Couldn't find end of MUC File.");
			fclose(fp);
			return false;
		}
		int bufsize = ftell(fp);
		if (bufsize == -1)
		{
			print("Couldn't get size of MUC File.");
			fclose(fp);
			return false;
		}

		if (fseek(fp, 0, SEEK_SET) != 0)
		{
			print("Couldn't get start of MUC File.");
			fclose(fp);
			//free(source);
			return false;
		}
		// Get File Data
		source = malloc(bufsize + 1);
		int newLen = fread(source, sizeof(char), bufsize, fp);
		if (newLen == 0)
		{
			trace("Error reading MUC file");
			fclose(fp);
			//free(source);
			return false;
		}
		source[newLen++] = '\0';
		fclose(fp);
		return _CRT_Compile(source);
		//free(source);
	}
	else if (_filecheck(filename, ".png"))
	{
	    cart_filename = filename;
		//Load External Cartridge
		int w, h;
		int pixel[CART_WIDTH*CART_WIDTH];
		int *cpixel;
		int error = lodepng_decode32_file((unsigned char **)(&cpixel), &w, &h, filename);
		if (error)
		{
			print("Cartridge Loading Error: %s", lodepng_error_text(error));
			return false;
		}
		if (w != CART_WIDTH || h != CART_WIDTH)
		{
			print("Provided file is not a valid Micro-8 cartridge.");
			free(cpixel);
			return false;
		}
		memcpy(pixel, cpixel, CART_WIDTH*CART_WIDTH*sizeof(int));
		free(cpixel);
		//Retrieve the Block Data.
		for (int i = 0; i < CART_SIZE/2; ++i)
		{
			unsigned int block = pixel[i] & 0x0f0f0f0f;
			unsigned char blocka = ((block >> 20) | (block >> 16)) & 0xFF;
			unsigned char blockb = ((block >> 4) | block) & 0xFF;
			cart.block[i*2] = blocka;
			cart.block[(i*2)+1] = blockb;
		}
	}

	return true;
}

char *CRT_GetCode()
{
	return cart.code;
}

char *CRT_GetTitle()
{
	return cart.title;
}

char *CRT_GetMusic()
{
    return cart.music;
}

char **CRT_GetSpriteMap()
{
	return (char**)cart.sprite;
}

char *CRT_GetMap()
{
	return cart.map;
}

void _CRT_DrawPixel(unsigned int *p, unsigned int a, unsigned char v)
{
	//Work out if we have something
	if ((v & 0x0F) == COLOR_TRANSPARENT)
		return;

	unsigned int l = HALF_BRIGHT;
	unsigned char i = v & 0x1;
	if (i)
		l = FULL_BRIGHT;
	unsigned char r = ((v & 8) >> 3) * l;
	unsigned char g = ((v & 4) >> 2) * l;
	unsigned char b = ((v & 2) >> 1) * l;

	p[a] = r | g << 8 | b << 16 | 0xFF << 24;// | 0xFFFFFFFF;
}

void CRT_SaveCart(const char *filename)
{
    print("Saving Cartridge Data to %s", (char *)filename);
    unsigned int error;
    unsigned int w, h;
    //Create Cart Rendering Surface
    //int *pixel = (int*)surface->pixels;
    int pixel[CART_WIDTH*CART_WIDTH];
    int *cpixel;
    //Render Card Image to Surface
    error = lodepng_decode32((unsigned char **)&cpixel, &w, &h, img_crt, sizeof(img_crt));
    if (error)
    {
        print("Internal Cartridge Image Load Failure: %s", lodepng_error_text(error));
        return;
    }
    memcpy(pixel, cpixel, CART_WIDTH*CART_WIDTH*sizeof(int));
    free(cpixel);
    print("Generated Default Cartridge Image.");

	//Render Unique Visuals to Cartridge
    char cgfx[VRAM_SIZE];
    VID_SetTarget(cgfx);
    //VID_Clear(COLOR_WHITE);
    VID_Clear(COLOR_TRANSPARENT);
    VID_SetDrawColor(COLOR_WHITE);
    //Add Cart Title to Cart Image
    char tx = (64-(strlen(cart.title)*CHAR_WIDTH))/2;
    TXT_PrintAt(cart.title, tx, 5);

    //Add Cart Icon to Cart Image
    VID_DrawSpriteScaled(60, 16, 16, 2, 2);
    VID_DrawSpriteScaled(61, 32, 16, 2, 2);
    VID_DrawSpriteScaled(62, 16, 32, 2, 2);
    VID_DrawSpriteScaled(63, 32, 32, 2, 2);

    //Render the Cart Gfx onto the cart
	VID_SetTarget(NULL);
	trace("Generated Etching Image.");
	for(int y = 0; y < CART_WIDTH; ++y)
	{
		for(int x = 0; x < (CART_WIDTH/2); ++x)
		{
			int i = (y * (CART_WIDTH/2)) + x;
			char a = (cgfx[i] & 0xF0) >> 4;
			char b = cgfx[i] & 0x0F;
			_CRT_DrawPixel(pixel, i*2, a);
			_CRT_DrawPixel(pixel, i*2 + 1, b);
		}
	}
	trace("Etched Cartridge.");
    //Render Code to Cart
    for (int i = 0; i < CART_SIZE/2; ++i)
    {
        unsigned int block = 0;
        unsigned char blocka = cart.block[i*2];
        unsigned char blockb = cart.block[(i*2)+1];
        block |= (blocka & 0xF0) << 20; //24-31
        block |= (blocka & 0x0F) << 16; //16-23
        block |= (blockb & 0xF0) << 4;  //8-15
        block |= (blockb & 0x0F);       //0-7
        pixel[i] = (pixel[i] & 0xf0f0f0f0) | (block & 0x0f0f0f0f);
    }
	trace("Imprinted Cartridge Data.");
    //Encode the Completed Image
    char *ext = strstr(filename, ".muc");
    if (ext)
        strcpy(ext, ".png");
    print("Saving Cartridge as %s", filename);
    error = lodepng_encode32_file(filename, (const unsigned char *)pixel, w, h);
    if(error)
        print("Cartridge Save Error: %s", lodepng_error_text(error));
    trace("Cartridge Saved.");
    trace("All Done.");
}

char CRT_User_GetChar(int id)
{
	if (id >= 0 && id < USER_SIZE)
	{
		return cart.user[id];
	}
	return '\0';
}

int CRT_User_GetInt(int id)
{
	if (id >= 0 && id < USER_SIZE - sizeof(int))
	{
		return *((int *)&cart.user[id]);
	}
	return 0;
}

const char *CRT_User_GetString(int id, int length)
{
	if (id >= 0 && id < USER_SIZE - length && length > 0)
	{
		char *tmp = malloc(length+1);
		strncpy(tmp, &(cart.user[id]), length);
		tmp[length] = '\0';
		return tmp;
	}
	return NULL;
}

int CRT_User_PutChar(int id, char v)
{
	if (id >= 0 && id < USER_SIZE)
	{
		cart.user[id] = v;
		return true;
	}
	return false;
}

int CRT_User_PutInt(int id, int v)
{
	if (id >= 0 && id < USER_SIZE - sizeof(int))
	{
		*((int *)&cart.user[id]) = v;
	}
	return false;
}

int CRT_User_PutString(int id, int l, const char *v)
{
	int length = strlen(v);
	if (length > l)
        length = l;
	if (length == 0)
		return true;
	if (id >= 0 && id < USER_SIZE - length && length > 0)
	{
		strncpy(&cart.user[id], v, length);
		return true;
	}
	return false;
}

void _CRT_GenChecksum(char check[USER_CHECKSIZE], char block[USER_SIZE])
{
    //Blank provided Checksum
    for(int i = 0; i < USER_CHECKSIZE; ++i)
    {
        check[i] = 0;
    }
    //Generate Checksum
    for(int i = 0; i < USER_SIZE; ++i)
    {
        int j = i%USER_CHECKSIZE;
        check[j] = check[j] ^ block[i];
    }
}

int CRT_User_Check()
{
    trace("Checked Checksum.");
	//Get new Checksum
	char temp[USER_CHECKSIZE];
	_CRT_GenChecksum(temp, cart.user);

	//Compare to current checksum
	for(int i = 0; i < USER_CHECKSIZE; ++i)
    {
        if (cart.check[i] != temp[i])
        {
            return false;
        }
    }
    return true;
}

int CRT_User_Sync()
{
    trace("Syncing User Data to Cartridge.");
	//Update internal checksum
	_CRT_GenChecksum(cart.check, cart.user);
	//Save Cart
	CRT_SaveCart(cart_filename);
	return true;
}

void CRT_Quit()
{
	//TODO - Close an External Cartridge
}
