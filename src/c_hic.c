//GlitchD1's Tiny Computer
//Human Interface Controller Commands

#include "gtc.h"
#include "hic.h"
#include "c_inc.h"

lil_value_t _C_HIC_IsKeyDown(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 1, "key")) return NULL;
    int key = HIC_KeyByName(lil_to_string(argv[0]));
	int out = false;
	if (key < HIC_MAXKEY)
    {
        out = HIC_GetKeyState(key);
    }
	return lil_alloc_integer(out);
}

lil_value_t _C_HIC_GetLastKey(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 0, "lastkey")) return NULL;
    return lil_alloc_string(HIC_NameByKey(HIC_GetLastKey()));
}

int C_HIC_Register(lil_t lil)
{
	lil_register(lil, "key", _C_HIC_IsKeyDown);
	lil_register(lil, "lastkey", _C_HIC_GetLastKey);
	return true;
}
