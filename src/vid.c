//GlitchD1's Tiny Computer
//Video System

#define DEBUG_CODE "VID"

//Internal
#include "gtc.h"
#include "debug.h"
#include "vid.h"

//External
#include "SDL.h"
#include <string.h>

//Variables
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
char vram[VRAM_SIZE];
char *target;
char sprite[SPRITE_MAX][SPRITE_SIZE];
char tilemap[MAP_SIZE];
char mapTable[MAPTABLE_SIZE] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
char drawColor;

int VID_Init(int psize)
{
	//Create Window
	window = SDL_CreateWindow( "GlitchD1's Tiny Computer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_SIZE*psize, SCREEN_SIZE*psize, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
	if (window == NULL)
		error("SDL was unable to create window: %s", SDL_GetError());

	//Create Renderer
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (renderer == NULL)
		error("SDL was unable to create renderer: %s", SDL_GetError());
	//Setup Renderer
	SDL_RenderSetLogicalSize(renderer, SCREEN_SIZE, SCREEN_SIZE);
	//SDL_RenderSetIntegerScale(renderer, true); -- Requires SDL2-GIT
	target = vram;
	drawColor = COLOR_WHITE;
	VID_Clear(COLOR_BLACK);
}


void VID_Static()
{
	//Scramble Screen Data
	for(int i = 0; i < VRAM_SIZE; ++i)
	{
		target[i] = rand() % 255;
	}
}

void VID_Clear(char color)
{
	//Set All Pixels to provided color
	color = (color & COLOR_WHITE) | (color << 4);
	for(int i = 0; i < VRAM_SIZE; ++i)
	{
		target[i] = color;
	}
}

void VID_SetPixel(char color, char x, char y)
{
    //	If Color is TRANSPARENT(0) then don't bother drawing it.
    color = color & drawColor;
	if (!color)
		return;

	//If Pixel is off-screen then also don't bother drawing it.
	if (x < 0 || x > SCREEN_SIZE-1 || y < 0 || y > SCREEN_SIZE-1)
		return;

	//Okay, all sanity checks done. Now set the pixel.
	int pos = (y * (SCREEN_SIZE/2)) + (x/2);
	target[pos] = hw_set(x, target[pos], color);
}

void _VID_DrawPixel(char x, char y, char v)
{
	//Work out if we have something
	int l = HALF_BRIGHT;
	char i = v & 0x1;
	if (i)
		l = FULL_BRIGHT;
	char r = ((v & 8) >> 3) * l;
	char g = ((v & 4) >> 2) * l;
	char b = ((v & 2) >> 1) * l;
	SDL_SetRenderDrawColor(renderer, r, g, b, 255);
	SDL_RenderDrawPoint(renderer, x, y);
}

void VID_SetSprite(char id, char data[SPRITE_SIZE])
{
	//TODO - Possibly Useful later on.
}

void VID_SetSpriteMap(char **data)
{
	memcpy(sprite, data, SPRITE_MAX*SPRITE_SIZE);
}

void VID_DrawSprite(char id, char x, char y)
{
	if (x < -7 || x > 63 || y < -7 || y > 63)
		return; //If it's not screen, we don't need to do anything.
	for (char j = 0; j < SPRITE_WIDTH; ++j)
	{
		for (char i = 0; i < SPRITE_WIDTH/2; ++i)
		{
			char pixel = (j * (SPRITE_WIDTH/2)) + i;
			char a = (sprite[id][pixel] >> 4) & 0x0F;
			char b = sprite[id][pixel] & 0x0F;
			VID_SetPixel(a, x + (i*2), y + j);
			VID_SetPixel(b, x + (i*2)+1, y + j);
		}
	}
}

void VID_DrawSpriteScaled(char id, char x, char y, char sx, char sy)
{
	if (x < (-8 * sx) + 1 || x > 63 || y < (-8 * sy) + 1 || y > 63)
		return; //If it's not screen, we don't need to do anything.
	for (char j = 0; j < SPRITE_WIDTH; ++j)
	{
		for (char i = 0; i < SPRITE_WIDTH/2; ++i)
		{
			char pixel = (j * (SPRITE_WIDTH/2)) + i;
			char a = (sprite[id][pixel] >> 4) & 0x0F;
			char b = sprite[id][pixel] & 0x0F;
			for(int sj = 0; sj < sy; ++sj)
			{
				for(int si = 0; si < sx; ++si)
				{
					VID_SetPixel(a, x + si + ((i*sx)*2), y + sj + (j*sy));
					VID_SetPixel(b, x + si + ((i*sx)*2)+sx, y + sj + (j*sy));
				}
			}

		}
	}
}

void VID_DrawMap(char x, char y, char mx, char my, char mw, char mh)
{
    int sx, sy, px, py;
    for(int j = 0; j < mh; ++j)
    {
        py = y + j * SPRITE_WIDTH;
        sy = my + j;
        if (sy < 0 || py < -SPRITE_WIDTH+1)
            continue;
        if (sy > MAP_WIDTH || py > SCREEN_SIZE-1)
            break;
        for(int i = 0; i < mw; ++i)
        {
            px = x + i * SPRITE_WIDTH;
            sx = mx + i/2;
            if (sx < 0 || px < -SPRITE_WIDTH+1)
                continue;
            if (sx > MAP_WIDTH || px > SCREEN_SIZE-1)
                break;
            int si = sx + (sy * (MAP_WIDTH/2));
            char spr = mapTable[hw_get(mx+i, tilemap[si])];
            VID_DrawSprite(spr, x + (i * SPRITE_WIDTH), y + (j * SPRITE_WIDTH));
        }
    }
}

void VID_SetMap(char *data)
{
	memcpy(tilemap, data, MAP_SIZE);
}

void VID_Render()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	//Update On-Screen Data
	for(int y = 0; y < SCREEN_SIZE; ++y)
	{
		for(int x = 0; x < (SCREEN_SIZE/2); ++x)
		{
			int i = (y * (SCREEN_SIZE/2)) + x;
			char a = (vram[i] & 0xF0) >> 4;
			char b = vram[i] & 0x0F;
			_VID_DrawPixel((x*2), y, a);
			_VID_DrawPixel((x*2) + 1, y, b);
		}
	}
	SDL_RenderPresent( renderer );
}

void VID_AssignMapSprite(char id, char sprite)
{
    if (id < 0 || id >= MAPTABLE_SIZE)
        return;
    if (sprite < 0 || sprite >= SPRITE_MAX)
        return;
    mapTable[id] = sprite;
}

char VID_GetMapSprite(char id)
{
    if (id < 0 || id >= MAPTABLE_SIZE)
        return 0;
    return mapTable[id];
}

void VID_SetTarget(char *t)
{
	if (t)
	{
		target = t;
	}
	else
	{
		target = vram;
	}
}

void VID_SetDrawColor(char color)
{
    drawColor = color & 0xF;
}

void VID_DrawHLine(char x1, char y, char x2)
{
    if (x2 < x1)
    {
        char tmp = x2;
        x2 = x1;
        x1 = tmp;
    }
    if (x1 < 0)
        x1 = 0;
    if (x2 > SCREEN_SIZE)
        x2 = SCREEN_SIZE;
    for (char i = x1; i <= x2; ++i)
    {
        VID_SetPixel(COLOR_WHITE, i, y);
    }
}

void VID_DrawVLine(char x, char y1, char y2)
{
    if (y2 < y1)
    {
        char tmp = y2;
        y2 = y1;
        y1 = tmp;
    }
    if (y1 < 0)
        y1 = 0;
    if (y2 > SCREEN_SIZE)
        y2 = SCREEN_SIZE;
    for (char i = y1; i <= y2; ++i)
    {
        VID_SetPixel(COLOR_WHITE, x, i);
    }
}

void VID_DrawLine(char x1, char y1, char x2, char y2)
{
    if (x1 == x2 && y1 == y2)
        VID_SetPixel(COLOR_WHITE, x1, y1);
    else if (y1 == y2)
        VID_DrawHLine(x1, y1, x2);
    else if (x1 == x2)
        VID_DrawVLine(x1, y1, y2);
    else
    {
    	int xs = 1;
    	int ys = 1;
        if (x2 < x1)
        {
            char tmp = x2;
            x2 = x1;
            x1 = tmp;
            xs = -xs;
        }
        if (y2 < y1)
        {
            char tmp = y2;
            y2 = y1;
            y1 = tmp;
            xs = -xs;
        }

        char xr = x2 - x1;
        char yr = y2 - y1;

        if (xr > yr)
        {
        	if (xs > 0)
        	{
		        for (char i = 0; i <= xr; ++i)
		        {
		            VID_SetPixel(COLOR_WHITE, x1 + i, y1 + (yr * i / xr));
		        }
            }
            else
            {
            	for (char i = 0; i <= xr; ++i)
		        {
		            VID_SetPixel(COLOR_WHITE, x1 + i, y2 - (yr * i / xr));
		        }
            }
        }
        else
        {
        	if (xs > 0)
        	{
		        for (char i = 0; i <= yr; ++i)
		        {
		            VID_SetPixel(COLOR_WHITE, x1 + (xr * i / yr), y1 + i);
		        }
            }
            else
            {
            	for (char i = 0; i <= yr; ++i)
		        {
		            VID_SetPixel(COLOR_WHITE, x2 - (xr * i / yr), y1 + i);
		        }
            }
        }
    }
}

void VID_DrawRect(char x, char y, char w, char h)
{
	if (w < 0 || h < 0)
		return;
	VID_DrawHLine(x, y, x+w-1);
	VID_DrawHLine(x, y+h-1, x+w-1);
	VID_DrawVLine(x, y, y+h-1);
	VID_DrawVLine(x+w-1, y, y+h-1);
}

void VID_DrawFRect(char x, char y, char w, char h)
{
	if (w < 0 || h < 0)
		return;
	for (int i = 0; i < h; ++i)
	{
		VID_DrawHLine(x, y+i, x+w-1);
	}
}

void VID_DrawCirc(char x, char y, char w, char h)
{
	// Uses Bresenham's Procedure for Ellipse Drawing.
	// GD1 - Eventually I'll understand the math behind this.
	// Set if we need to add pixels later on, as Bresenham doesn't normally do even sized things.
	int ls = (w+1) % 2;
	int ds = (h+1) % 2;

	// Adjust Parameters to fit expectations
	w = (w-1)/2;
	h = (h-1)/2;
	x += w;
	y += h;

	// Variable Setup
	int a2 = w * w;
	int b2 = h * h;
	int fa2 = 4 * a2;
	int fb2 = 4 * b2;
	int px, py, s;

	// First Half
	for (px = 0, py = h, s = 2*b2+a2*(1-2*h); b2*px <= a2*py; ++px)
	{
		VID_SetPixel(COLOR_WHITE, x + px + ls, y + py + ds);
		VID_SetPixel(COLOR_WHITE, x - px, y + py + ds);
		VID_SetPixel(COLOR_WHITE, x + px + ls, y - py);
		VID_SetPixel(COLOR_WHITE, x - px, y - py);

		if (s >= 0)
		{
			s += fa2 * (1 - py);
			--py;
		}
		s += b2 * ((4 * px) + 6);
	}

	// Second Half
	for (px = w, py = 0, s = 2*a2+b2*(1-2*w); a2*py <= b2*px; ++py)
	{
		VID_SetPixel(COLOR_WHITE, x + px + ls, y + py + ds);
		VID_SetPixel(COLOR_WHITE, x - px, y + py + ds);
		VID_SetPixel(COLOR_WHITE, x + px + ls, y - py);
		VID_SetPixel(COLOR_WHITE, x - px, y - py);
		if (s >= 0)
		{
			s += fb2 * (1 - px);
			--px;
		}
		s += a2 * ((4 * py) + 6);
	}
}

void VID_DrawFCirc(char x, char y, char w, char h)
{
	// Uses Bresenham's Procedure for Ellipse Drawing.
	// GD1 - Eventually I'll understand the math behind this.
	// Set if we need to add pixels later on, as Bresenham doesn't normally do even sized things.
	int ls = (w+1) % 2;
	int ds = (h+1) % 2;

	// Adjust Parameters to fit expectations
	w = (w-1)/2;
	h = (h-1)/2;
	x += w;
	y += h;

	// Variable Setup
	int a2 = w * w;
	int b2 = h * h;
	int fa2 = 4 * a2;
	int fb2 = 4 * b2;
	int px, py, s;

	// First Half
	for (px = 0, py = h, s = 2*b2+a2*(1-2*h); b2*px <= a2*py; ++px)
	{
		//TODO - Significantly reduce the overdraw happening here.
		VID_DrawHLine(x + px + ls, y + py + ds, x - px);
		VID_DrawHLine(x + px + ls, y - py, x - px);

		if (s >= 0)
		{
			s += fa2 * (1 - py);
			--py;
		}
		s += b2 * ((4 * px) + 6);
	}

	// Second Half
	for (px = w, py = 0, s = 2*a2+b2*(1-2*w); a2*py <= b2*px; ++py)
	{
		//TODO - Significantly reduce the overdraw happening here.
		VID_DrawHLine(x + px + ls, y + py + ds, x - px);
		VID_DrawHLine(x + px + ls, y - py, x - px);

		if (s >= 0)
		{
			s += fb2 * (1 - px);
			--px;
		}
		s += a2 * ((4 * py) + 6);
	}
}

void VID_Quit()
{
	if (renderer)
		SDL_DestroyRenderer( renderer );
	if (window)
		SDL_DestroyWindow( window );
}
