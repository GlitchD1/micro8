//GlitchD1's Tiny Computer
//Global Variables

#define DEBUG_CODE "GTC"

#include "gtc.h"
#include "debug.h"
#include "vid.h"
#include "txt.h"
#include "snd.h"
#include "cpu.h"
#include "hic.h"
#include "crt.h"

#include "SDL.h"
#include <time.h>
#include <stdlib.h>

int main(int argc, char* args[])
{
    //Seed Randomizer
    srand(time(NULL));
	//Handle Command Line
	char *filename = NULL;
	bool defaultCart = false;
	bool saveCart = false;
	trace("Arguments Given: %d", argc-1);
	for(int i = 1; i < argc; ++i)
    {
        if (strcmp(args[i], "-dc") == 0)
        {
            trace("Forcing Default Cartridge.");
            defaultCart = true;
        }
        else if (strcmp(args[i], "-sc") == 0)
        {
            trace("Saving Cartridge.");
            saveCart = true;
        }
        else
        {
            trace("Filename Given: %s", args[i]);
            filename = args[i];
        }
    }
	char sc = COLOR_BLACK;
	//Ensure SDL is running.
	if (SDL_Init( SDL_INIT_VIDEO || SDL_INIT_AUDIO ) < 0)
	{
		error("SDL Failed to Init: %s",SDL_GetError());
	}

	//Initialise our bits
	bool active = true;
	VID_Init(8);
	TXT_Init();
	SND_Init();
	HIC_Init();
	CPU_Init();
	if (defaultCart)
	{
	    active = CRT_Init(NULL);
	}
    else
    {
        active = CRT_Init(filename);
    }
    SND_Sheet_SetAll(CRT_GetMusic());
    VID_SetSpriteMap(CRT_GetSpriteMap());
    VID_SetMap(CRT_GetMap());
    if (saveCart)
    {
        CRT_SaveCart(filename);
    }
	SDL_Event e;

	char *title;
	if (active)
		title = CRT_GetTitle();
	else
		title = "Invalid Cart";
	bool bactive = true;
	int ws = (TITLE_MAX_LENGTH - strlen(title))/2;

	//Run Boot Screen
	VID_Clear(COLOR_NAVY);
	TXT_Print(" G-Tech MICRO-8 ");
	TXT_Print("----------------\n\n\n\n");
	for (int i = 0; i < ws;++i)
	{
		TXT_Print(" ");
	}
	TXT_Print(title);
	if(strlen(title) < TITLE_MAX_LENGTH)
	{
		TXT_Print("\n");
	}
	TXT_Print("----------------");
	TXT_Print(" Press any key\n");
	TXT_Print("   to start\n");
    VID_DrawSprite(60, 24, 16);
    VID_DrawSprite(61, 32, 16);
    VID_DrawSprite(62, 24, 24);
    VID_DrawSprite(63, 32, 24);
	while(bactive)
	{
		while( SDL_PollEvent( &e ) != 0)
		{
			if( e.type == SDL_QUIT)
			{
				active = false;
				bactive = false;
			}
			if( e.type == SDL_KEYDOWN && e.key.repeat == 0)
			{
				if(e.key.keysym.sym == SDLK_ESCAPE)
				{
					active = false;
				}
				bactive = false;
			}
		}
		VID_Render();
	}

	//Run Setup phase of cartridge
	CPU_Parse(CRT_GetCode());
	VID_Render();

	//Event Loop
	int next_sync = SDL_GetTicks() + FRAME_RATE;
	while( active )
	{
		//Event Polling
		while( SDL_PollEvent( &e ) != 0)
		{
			if( e.type == SDL_QUIT)
			{
				active = false;
			}
			else if( e.type == SDL_KEYDOWN && e.key.repeat == 0)
			{
				if(e.key.keysym.sym == SDLK_ESCAPE)
				{
					active = false;
				}
				else if (e.key.keysym.sym < HIC_MAXKEY)
                {
                    HIC_KeyPressed(e.key.keysym.sym);
                }
			}
			else if( e.type == SDL_KEYUP)
            {
                if (e.key.keysym.sym < HIC_MAXKEY)
                {
                    HIC_KeyReleased(e.key.keysym.sym);
                }
            }
		}

		//Before Sync Chip Calls
		VID_SetDrawColor(COLOR_WHITE);
		active = active && CPU_Loop();
		VID_Render();

		//Sync to FRAME_RATE
		int ticks = SDL_GetTicks();
		if (ticks < next_sync)
			SDL_Delay(next_sync - ticks);
		next_sync = next_sync + FRAME_RATE;

		//After Sync Chip Calls
		SND_Update();
	}

	// Shutdown Everything Nicely and leave...
	CPU_Quit();
	HIC_Quit();
	SND_Quit();
	VID_Quit();
	SDL_Quit();
	return 0;
}
