//GlitchD1's Tiny Computer
//Video System Commands

#include "gtc.h"
#include "vid.h"
#include "txt.h"
#include "c_inc.h"

lil_value_t _C_VID_Static(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 0, "static")) return NULL;
    //Static up the screen
	VID_Static();
	return NULL;
}

lil_value_t _C_VID_ClearScreen(lil_t lil, size_t argc,lil_value_t* argv)
{
	if (LIL_ParaMax(lil, argc, 1, "cls")) return NULL;
    //Set the screen color
    char color = COLOR_BLACK;
	if (argc == 1)
	{
		color = (char)(lil_to_integer(argv[0])) & COLOR_WHITE;
	}
	VID_Clear(color);
	TXT_Reset();
	return NULL;
}

lil_value_t _C_VID_DrawPixel(lil_t lil, size_t argc,lil_value_t* argv)
{
	if (LIL_ParaCount(lil, argc, 3, "dot")) return NULL;
    //Draw a pixel
	char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char c = lil_to_integer(argv[2]);
	VID_SetPixel(c, x, y);
	return NULL;
}

lil_value_t _C_VID_DrawSprite(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 3, "draw")) return NULL;
	//Draw a sprite
    char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char i = lil_to_integer(argv[2]);
	VID_DrawSprite(i, x, y);
	return NULL;
}

lil_value_t _C_VID_DrawMap(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaRange(lil, argc, 4, 6, "map")) return NULL;
    //Draw a tile map
	char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char mx = lil_to_integer(argv[2]);
	char my = lil_to_integer(argv[3]);
	char mw = 8;
	char mh = 8;
	if (argc == 5)
    {
        mw = mh = lil_to_integer(argv[4]);
    }
    else if (argc == 6)
	{
        mw = lil_to_integer(argv[4]);
        mh = lil_to_integer(argv[5]);
    }
	VID_DrawMap(x, y, mx, my, mw, mh);
	return NULL;
}

lil_value_t _C_VID_AssignMapSprite(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 2, "ams")) return NULL;
	//Assign a Map Sprite to an ID.
	char i = lil_to_integer(argv[0]);
	char s = lil_to_integer(argv[1]);
	VID_AssignMapSprite(i, s);
	return NULL;
}

lil_value_t _C_VID_GetMapSprite(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 1, "gms")) return NULL;
	//Get Map Sprite for Map ID
	char i = lil_to_integer(argv[0]);
	return lil_alloc_integer(VID_GetMapSprite(i));
}

lil_value_t _C_VID_SetColor(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 1, "color")) return NULL;
    //Set the current draw color
    char c = lil_to_integer(argv[0]);
    VID_SetDrawColor(c);
	return NULL;
}

lil_value_t _C_VID_DrawLine(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 4, "line")) return NULL;
	//Draw a line
    char x1 = lil_to_integer(argv[0]);
    char y1 = lil_to_integer(argv[1]);
    char x2 = lil_to_integer(argv[2]);
    char y2 = lil_to_integer(argv[3]);
    VID_DrawLine(x1, y1, x2, y2);
	return NULL;
}

lil_value_t _C_VID_DrawHLine(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 3, "hline")) return NULL;
	//Draw a horizontal line
	char x1 = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char x2 = lil_to_integer(argv[2]);
	VID_DrawHLine(x1, y, x2);
	return NULL;
}

lil_value_t _C_VID_DrawVLine(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 3, "vline")) return NULL;
	//Draw a vertical line
    char x = lil_to_integer(argv[0]);
	char y1 = lil_to_integer(argv[1]);
	char y2 = lil_to_integer(argv[2]);
	VID_DrawVLine(x, y1, y2);
	return NULL;
}

lil_value_t _C_VID_DrawRect(lil_t lil, size_t argc, lil_value_t* argv)
{
	if (LIL_ParaCount(lil, argc, 4, "rect")) return NULL;
	//Draw a rectangle
    char x = lil_to_integer(argv[0]);
    char y = lil_to_integer(argv[1]);
    char w = lil_to_integer(argv[2]);
    char h = lil_to_integer(argv[3]);
    VID_DrawRect(x, y, w, h);
	return NULL;
}

lil_value_t _C_VID_DrawFRect(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 4, "frect")) return NULL;
    //Draw a filled rectangle
    char x = lil_to_integer(argv[0]);
    char y = lil_to_integer(argv[1]);
	char w = lil_to_integer(argv[2]);
	char h = lil_to_integer(argv[3]);
	VID_DrawFRect(x, y, w, h);
	return NULL;
}

lil_value_t _C_VID_DrawCirc(lil_t lil, size_t argc, lil_value_t* argv)
{
	//Draw an ellipse
	if (LIL_ParaCount(lil, argc, 4, "circ")) return NULL;
    char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char w = lil_to_integer(argv[2]);
	char h = lil_to_integer(argv[3]);
	VID_DrawCirc(x, y, w, h);
	return NULL;
}

lil_value_t _C_VID_DrawFCirc(lil_t lil, size_t argc, lil_value_t* argv)
{
	if (LIL_ParaCount(lil, argc, 4, "fcirc")) return NULL;
    //Draw a filled ellipse
    char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	char w = lil_to_integer(argv[2]);
	char h = lil_to_integer(argv[3]);
	VID_DrawFCirc(x, y, w, h);
	return NULL;
}

int C_VID_Register(lil_t lil)
{
	lil_register(lil, "static", _C_VID_Static);
	lil_register(lil, "cls", _C_VID_ClearScreen);
	lil_register(lil, "dot", _C_VID_DrawPixel);
	lil_register(lil, "draw", _C_VID_DrawSprite);
	lil_register(lil, "map", _C_VID_DrawMap);
	lil_register(lil, "ams", _C_VID_AssignMapSprite);
	lil_register(lil, "gms", _C_VID_GetMapSprite);
	lil_register(lil, "color", _C_VID_SetColor);
	lil_register(lil, "line", _C_VID_DrawLine);
	lil_register(lil, "hline", _C_VID_DrawHLine);
	lil_register(lil, "vline", _C_VID_DrawVLine);
	lil_register(lil, "rect", _C_VID_DrawRect);
	lil_register(lil, "frect", _C_VID_DrawFRect);
	lil_register(lil, "circ", _C_VID_DrawCirc);
	lil_register(lil, "fcirc", _C_VID_DrawFCirc);
	return true;
}
