//GlitchD1's Tiny Computer
//Human Interface Controller

#define DEBUG_CODE "HIC"

#include "gtc.h"
#include "debug.h"
#include "hic.h"

#include "SDL.h"

bool keymap[HIC_MAXKEY];
char lastKey;

int HIC_Init()
{
    for(int i = 0; i < HIC_MAXKEY; ++i)
    {
        keymap[i] = false;
    }
    lastKey = 0;
	return true;
}

int HIC_GetKeyState(char key)
{
	return keymap[key];
}

void HIC_KeyPressed(char key)
{
    keymap[key] = true;
    lastKey = key;
}

void HIC_KeyReleased(char key)
{
    keymap[key] = false;
}

char HIC_GetLastKey()
{
    return lastKey;
}

char HIC_KeyByName(const char* name)
{
    return SDL_GetKeyFromName(name);
}

const char *HIC_NameByKey(char key)
{
    return SDL_GetKeyName(key);
}

void HIC_Quit()
{
    //TODO - In case I add any devices that may need closing.
}
