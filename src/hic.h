//GlitchD1's Tiny Computer
//Human Interface Controller Header

#define HIC_MAXKEY 128

int HIC_Init();

int HIC_GetKeyState(char key);

void HIC_KeyPressed(char key);

void HIC_KeyReleased(char key);

char HIC_GetLastKey();

char HIC_KeyByName(const char* name);

const char *HIC_NameByKey(char key);

void HIC_Quit();
