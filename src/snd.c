//GlitchD1's Tiny Computer
//Sound System Header

#define DEBUG_CODE "SND"

#include "gtc.h"
#include "snd.h"
#include "debug.h"

#include "SDL.h"
#include "ext_bb.h"

#define SAMPLE_RATE			48000
#define SOUNDBUFFER_SIZE	1024
#define PERIOD_MULTI		200
#define PERIOD_BASE			800
#define PHASE_RANGE			16
#define PHASE_HALF			16

double clock_rate = 1024000.0;//(double) SAMPLE_RATE * blip_max_ratio;

blip_t *blip;
SDL_AudioSpec as = { SAMPLE_RATE, AUDIO_S16SYS, 1 };

typedef struct _channel {
	unsigned char note;
	short position;
	short start;
	short end;
	short tempo;
	short time;
	unsigned char volume;
	int next;
	int amp;
	int phase;
	char timbre;
	char bank;
} _channel;

char note_sheet[SHEET_NOTES];

_channel channels[CHANNEL_NUM];

char _SND_GetSheetNote(short pos)
{
	return (note_sheet[pos/2] >> (4*(pos%2))) & 0x0F;
}

int _SND_Delta(_channel *c, int period, int volume, int phase)
{
	int delta = (((phase-PHASE_HALF) * volume)) - c->amp;
	c->amp += delta;
	blip_add_delta(blip, c->next, delta);
	c->next += period;
}

void _SND_BufferFill( void* unused, Uint8* out, int byte_count)
{
	int samples = byte_count / sizeof (short);
	int clocks = blip_clocks_needed(blip, samples);
	for(int i = 0; i < CHANNEL_NUM; ++i)
	{
		_channel *c = &channels[i];

		if (c->time == 0)
		{
			//c->amp = 0;
			c->next = 0;
			continue;
		}

		char type = c->note >> 6;
		char nper = c->note & (NOTE_PERIOD_MAX-1);
		int period = (int)((NOTE_PERIOD_MAX-nper)*PERIOD_MULTI) + PERIOD_BASE;
		int volume = (int)(c->volume) * (16 / CHANNEL_NUM);
		while( c->next < clocks)
		{
			//int delta;
			switch(type)
			{
			//Square Wave
			case NOTE_TYPE_SQUARE:
				_SND_Delta(c, period/PHASE_RANGE, volume, (c->phase < c->timbre) ? 0 : PHASE_RANGE);
				c->phase = (c->phase + 1) % PHASE_RANGE;
				break;
			//Triangle Wave
			case NOTE_TYPE_TRIANGLE:
				_SND_Delta(c, period/PHASE_RANGE, volume, (c->phase < PHASE_RANGE) ? c->phase : ((PHASE_RANGE*2)-1) - c->phase);
				c->phase = (c->phase + 1) % (PHASE_RANGE*2);
				break;
			//Sawtooth Wave
			case NOTE_TYPE_SAWTOOTH:
				_SND_Delta(c, period/PHASE_RANGE, volume, c->phase);
				c->phase = (c->phase + 1) % (PHASE_RANGE);
				break;
			//Noise Wave
			case NOTE_TYPE_NOISE:
				if (c->phase == 0) //LFSR Noise cannot be zero.
					c->phase = 1;
				_SND_Delta(c, period, volume, (c->phase & 1) * PHASE_RANGE);
				c->phase = ((c->phase & 1) * c->timbre) ^ (c->phase >> 1);
				break;
			}
		}
		c->next -= clocks;
	}
	blip_end_frame(blip, clocks);

	//Output channel
	SDL_memset(out, 0, byte_count);

	blip_read_samples(blip, (short *)out, samples, 0);
}

int SND_Init()
{
	//Preps Channels for use.
	for(int i = 0; i < CHANNEL_NUM; ++i)
	{
		channels[i].note = 0;
		channels[i].time = 0;
		channels[i].volume = 255;
		channels[i].next = 0;
		channels[i].phase = 0;
		channels[i].amp = 0;
		channels[i].timbre = 12;//(i * 4) + 1;
		channels[i].tempo = 10;
		channels[i].start = 0;
		channels[i].end = 0;
		channels[i].position = 0;
		channels[i].bank = 0;
	}

	for(int i = 0; i < SHEET_NOTES; ++i)
	{
		note_sheet[i] = 0;
	}

	//Activate Blip Buffer
	blip = blip_new(SAMPLE_RATE/10);
	if (blip == NULL)
		error("Blipbuffer Wave Former Stopped working.");

	blip_set_rates(blip, clock_rate, SAMPLE_RATE);

	//Activate SDL Sound.
	as.callback = _SND_BufferFill;
	as.samples = 1024;
	as.format = AUDIO_S16;
	as.channels = 1;
	if( SDL_OpenAudio( &as, 0) < 0)
		error("SDL had trouble opening access to audio: %s", SDL_GetError());

	SDL_PauseAudio( 0 );
}

char SND_Note_Create(char type, char period)
{
	//Convert a type and period into a note byte code.
	return (type << 6) | (period & 0x3F);
}

void SND_Channel_Volume(char channel, char volume)
{
	if (channel < 0 || channel >= CHANNEL_NUM)
		error("Attempting to set volume on invalid channel");
	channels[channel].volume = volume;
}

void SND_Channel_PlayFromBank(char channel, short start, short end, short beat)
{
	//TODO - Once we have a note bank and channels.
	if (channel < 0 || channel > CHANNEL_NUM-1)
		error("Missing Channel for Bank Play");

	if (start < 0 || start > SHEET_NOTES)
		error("Start exists outside Sheet");

	if (end < 0 || end > SHEET_NOTES)
		error("End exists outside Sheet");

	channels[channel].note = note_sheet[start];
	channels[channel].time = beat;
	channels[channel].tempo = beat;
	channels[channel].start = start;
	channels[channel].end = end;
	channels[channel].position = start;
}

void SND_Channel_PlaySingle(char channel, char note, short time)
{
	if (channel < 0 || channel > CHANNEL_NUM-1)
		error("Missing Channel for Single Play");

	channels[channel].note = note;
	channels[channel].time = time;
	channels[channel].start = 0;
	channels[channel].end = 0;
	channels[channel].position = 0;
}

void SND_Sheet_SetNote(short pos, char note)
{
	if (pos < 0 || pos > SHEET_NOTES)
		error("Writing off-sheet.");
	note_sheet[pos] = note; //hw_set(pos, note_sheet[pos], note);
}

void SND_Sheet_SetAll(char notes[SHEET_NOTES])
{
	memcpy(note_sheet, notes, SHEET_NOTES);
}

void SND_Update()
{
	for(int i = 0; i < CHANNEL_NUM; ++i)
	{
		if (channels[i].time > 0)
		{
			--channels[i].time;
			if(channels[i].time == 0 && channels[i].position != channels[i].end)
			{
				channels[i].position++;
				channels[i].note = note_sheet[channels[i].position];
				channels[i].time = channels[i].tempo;
			}
		}
	}
}

void SND_Quit()
{
	SDL_PauseAudio( 1 );
	blip_delete( blip );
}
