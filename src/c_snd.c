//GlitchD1's Tiny Computer
//Video System Commands

#include "gtc.h"
#include "snd.h"
#include "c_inc.h"

lil_value_t _C_SND_SetVolume(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 2, "vol")) return NULL;
    //Set a channels volume
	char c = (char)(lil_to_integer(argv[0]));
	char v = (char)(lil_to_integer(argv[1]));
	SND_Channel_Volume(c,v);
	return NULL;
}

lil_value_t _C_SND_PlayFromBank(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 4, "play")) return NULL;
    //Play audio from the bank
	char c = (char)(lil_to_integer(argv[0]));
	char s = (char)(lil_to_integer(argv[1]));
	char e = (char)(lil_to_integer(argv[2]));
	char t = (char)(lil_to_integer(argv[3]));

	SND_Channel_PlayFromBank(c, s, e, t);
	return NULL;
}

lil_value_t _C_SND_PlaySingle(lil_t lil, size_t argc,lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 3, "note")) return NULL;
    //Play a single note
	char c = (char)(lil_to_integer(argv[0]));
	char n = (char)(lil_to_integer(argv[1]));
	char t = (char)(lil_to_integer(argv[2]));

	SND_Channel_PlaySingle(c, n, t);
	return NULL;
}

int C_SND_Register(lil_t lil)
{
	lil_register(lil, "vol", _C_SND_SetVolume);
	lil_register(lil, "play", _C_SND_PlayFromBank);
	lil_register(lil, "note", _C_SND_PlaySingle);
	return true;
}
