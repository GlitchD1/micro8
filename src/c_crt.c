//GlitchD1's Tiny Computer
//Cartridge Access Commands

#include "gtc.h"
#include "crt.h"
#include "c_inc.h"

lil_value_t _C_CRT_Sync(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 0, "sync")) return NULL;
    //Sync Data to cartridge
	CRT_User_Sync();
	return NULL;
}

lil_value_t _C_CRT_Check(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 0, "check")) return NULL;
    //Check Data against cartridge
    int check = CRT_User_Check();
    return lil_alloc_integer(check);
}

lil_value_t _C_CRT_PutChar(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 2, "putc")) return NULL;
    //Put a single byte into user data
    int i = lil_to_integer(argv[0]);
	char c = lil_to_integer(argv[1]);
	CRT_User_PutChar(i, c);
	return NULL;
}

lil_value_t _C_CRT_PutInt(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 2, "puti")) return NULL;
    //Put a 4 byte integer into user data
    int i = lil_to_integer(argv[0]);
	int c = lil_to_integer(argv[1]);
	CRT_User_PutInt(i, c);
	return NULL;
}

lil_value_t _C_CRT_PutString(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 3, "puts")) return NULL;
    //Put a string into user data
    int i = lil_to_integer(argv[0]);
	int l = lil_to_integer(argv[0]);
	const char *c = lil_to_string(argv[1]);
	CRT_User_PutString(i, l, c);
	return NULL;
}

lil_value_t _C_CRT_GetChar(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 1, "getc")) return NULL;
    //Get a character from userdata
    int i = lil_to_integer(argv[0]);
	char v = CRT_User_GetChar(i);
	return lil_alloc_integer(v);
}

lil_value_t _C_CRT_GetInt(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 1, "getc")) return NULL;
    //Get a 4-byte integer from userdata
    int i = lil_to_integer(argv[0]);
	int v = CRT_User_GetInt(i);
	return lil_alloc_integer(v);
}

lil_value_t _C_CRT_GetString(lil_t lil, size_t argc, lil_value_t* argv)
{
    if (LIL_ParaCount(lil, argc, 2, "gets")) return NULL;
    //Get a string from userdata
    int i = lil_to_integer(argv[0]);
	int l = lil_to_integer(argv[1]);
	const char *v = CRT_User_GetString(i, l);
	return lil_alloc_string(v);
}

int C_CRT_Register(lil_t lil)
{
	lil_register(lil, "sync", _C_CRT_Sync);
	lil_register(lil, "check", _C_CRT_Check);
	lil_register(lil, "putc", _C_CRT_PutChar);
	lil_register(lil, "puti", _C_CRT_PutInt);
	lil_register(lil, "puts", _C_CRT_PutString);
	lil_register(lil, "getc", _C_CRT_GetChar);
	lil_register(lil, "geti", _C_CRT_GetInt);
	lil_register(lil, "gets", _C_CRT_GetString);
	return true;
}

