//GlitchD1's Tiny Computer
//Text Commands

#include "gtc.h"
#include "txt.h"
#include "c_inc.h"

lil_value_t _C_TXT_Print(lil_t lil, size_t argc, lil_value_t* argv)
{
    //Print text at cursor
	if (argc == 0)
		TXT_Print("\n");
	else
	{
		for(int i = 0; i < argc; ++i)
		{
			const char *text = lil_to_string(argv[i]);
			TXT_Print(text);
		}
	}
	return NULL;
}

lil_value_t _C_TXT_PrintLine(lil_t lil, size_t argc, lil_value_t* argv)
{
    //Print text at cursor, then newline
	for(int i = 0; i < argc; ++i)
	{
		const char *text = lil_to_string(argv[i]);
		TXT_Print(text);
	}
	TXT_Print("\n");
	return NULL;
}


lil_value_t _C_TXT_PrintAt(lil_t lil, size_t argc, lil_value_t* argv)
{
	if(LIL_ParaCount(lil, argc, 3, "printat")) return NULL;
	//Print text at x,y
	char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	const char *text = lil_to_string(argv[2]);
	TXT_PrintAt(text, x, y);
	return NULL;
}

lil_value_t _C_TXT_SetCursor(lil_t lil, size_t argc, lil_value_t* argv)
{
    if(LIL_ParaCount(lil, argc, 2, "setcur")) return NULL;
    //Set the Text Cursor position
    char x = lil_to_integer(argv[0]);
	char y = lil_to_integer(argv[1]);
	TXT_SetCursor(x, y);
	return NULL;
}

int C_TXT_Register(lil_t lil)
{
	lil_register(lil, "print", _C_TXT_Print);
	lil_register(lil, "println", _C_TXT_PrintLine);
	lil_register(lil, "printat", _C_TXT_PrintAt);
	lil_register(lil, "setcur", _C_TXT_SetCursor);

	return true;
}
