//GlitchD1's Tiny Computer
//Font and Text Drawing Header

#define CHARS			128
#define CHAR_WIDTH		4
#define CHAR_HEIGHT		6
#define CHAR_BITS		24
#define CHARS_PER_LINE	16
#define MAX_LINES		10

bool TXT_Init();

void TXT_Print(const char *text);

void TXT_PrintAt(const char *text, char x, char y);

void TXT_SetCursor(char x, char y);

void TXT_Reset();

void TXT_DrawChar(unsigned char symbol, char x, char y);

void TXT_Quit();
