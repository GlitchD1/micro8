//GlitchD1's Tiny Computer
//Video System Header

//Defines
#define VRAM_SIZE       2048 //64*64 half-bytes (or 64*32 bytes)
#define SPRITE_SIZE     32
#define SPRITE_MAX      64
#define SPRITE_WIDTH	8
#define MAP_SIZE		512 //32x32 half-bytes
#define MAP_WIDTH		32
#define MAPTABLE_SIZE   16

#define FULL_BRIGHT			0xFF
#define HALF_BRIGHT			0x88

//Color Definitions			    //RGBI
#define COLOR_TRANSPARENT	0x0 //0000
#define COLOR_BLACK			0x1 //0001
#define COLOR_NAVY			0x2 //0010
#define COLOR_BLUE			0x3 //0011
#define COLOR_GREEN			0x4 //0100
#define COLOR_LIME			0x5 //0101
#define COLOR_CYAN			0x6 //0110
#define COLOR_AQUA			0x7 //0111
#define COLOR_MAROON		0x8 //1000
#define COLOR_RED			0x9 //1001
#define COLOR_PURPLE		0xA //1010
#define COLOR_PINK			0xB //1011
#define COLOR_OLIVE			0xC //1100
#define COLOR_YELLOW		0xD //1101
#define COLOR_GRAY			0xE //1110
#define COLOR_WHITE			0xF //1111
#define COLOR_NUM			16

int VID_Init();

void VID_Static();

void VID_Clear(char color);

void VID_SetPixel(char color, char x, char y);

void VID_SetSprite(char id, char data[SPRITE_SIZE]);

void VID_SetSpriteMap(char **data);

void VID_DrawSprite(char id, char x, char y);

void VID_DrawSpriteScaled(char id, char x, char y, char sx, char sy);

void VID_DrawMap(char x, char y, char mx, char my, char mw, char mh);

void VID_SetMap(char *data);

void VID_AssignMapSprite(char id, char sprite);

char VID_GetMapSprite(char id);

void VID_Render();

void VID_SetTarget(char *t);

void VID_SetDrawColor(char color);

void VID_DrawHLine(char x1, char y1, char x2);
void VID_DrawVLine(char x1, char y1, char y2);
void VID_DrawLine(char x1, char y1, char x2, char y2);
void VID_DrawRect(char x, char y, char w, char h);
void VID_DrawFRect(char x, char y, char w, char h);
void VID_DrawCirc(char x, char y, char w, char h);
void VID_DrawFCirc(char x, char y, char w, char h);

void VID_Quit();
