//GlitchD1's Tiny Computer
//Internal Debugger Header

//#define ERRI_UNKNOWN	0
//#define ERRI_SDL		1
//#define ERRI_BLIP		2
//#define ERRI_SND		3
//#define ERRI_MAX		4

#define ERRL_TOO_MANY		"Too Many Parameters"
#define ERRL_WRONG_PARAS	"Wrong Number of Parameters"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef DEBUG_CODE
#define DEBUG_CODE "Unknown"
#endif // DEBUG_CODE

#ifdef DEBUG
static inline void trace(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("T: %s: ", DEBUG_CODE);
    vprintf(format, args);
    putchar('\n');
    va_end(args);
}
#else
static inline void trace(...) {}
#endif
static inline void error(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("ERR: %s: ", DEBUG_CODE);
    vprintf(format, args);
    va_end(args);
    putchar('\n');
    exit(1);
}

static inline void print(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("%s: ", DEBUG_CODE);
    vprintf(format, args);
    putchar('\n');
    va_end(args);
}

//Old Trace System
//void error(int type, char *msg);
//void trace(const char *msg);
//void traci(const char *msg, int val);
//void tracu(const char *msg, char val);
//void tracs(const char *msg, const char *val);
