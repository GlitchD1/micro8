//GlitchD1's Tiny Computer
//LIL command helpers

#define DEBUG_CODE "LIL"
#include "debug.h"
#include "ext_lil.h"

//NOTE: These return true if a test has failed. Mainly as putting ! before every single one wouldn't look that great.

static inline int LIL_ParaCount(lil_t lil, int count, int required, char *command)
{
    if (count == required)
        return false;

    char buffer[1000];
    sprintf(buffer, "%s requires exactly %d arguments, instead of %d.", command, required, count);
    print(buffer);
    lil_set_error(lil, buffer);
    return true;
}

static inline int LIL_ParaRange(lil_t lil, int count, int minc, int maxc, char *command)
{
    if (count >= minc && count <= maxc)
        return false;

    char buffer[1000];
    sprintf(buffer, "%s requires between %d and %d arguments, instead of %d.", command, minc, maxc, count);
    print(buffer);
    lil_set_error(lil, buffer);
    return true;
}

static inline int LIL_ParaMin(lil_t lil, int count, int minc, char *command)
{
    if (count >= minc)
        return false;

    char buffer[1000];
    sprintf(buffer, "%s requires at least %d arguments, instead of %d.", command, minc, count);
    print(buffer);
    lil_set_error(lil, buffer);
    return true;
}

static inline int LIL_ParaMax(lil_t lil, int count, int maxc, char *command)
{
    if (count <= maxc)
        return false;

    char buffer[1000];
    sprintf(buffer, "%s requires at most %d arguments, instead of %d.", command, maxc, count);
    print(buffer);
    lil_set_error(lil, buffer);
    return true;
}
