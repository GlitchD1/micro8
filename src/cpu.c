//GlitchD1's Tiny Computer
//Code Processing Unit

#define DEBUG_CODE "CPU"

#include "gtc.h"
#include "cpu.h"
#include "txt.h"
#include "vid.h"
#include "debug.h"
#include "ext_lil.h"
#include <stdio.h>
#include <string.h>

lil_t lil;

bool cpu_lock = false;

int C_VID_Register(lil_t lil);
int C_TXT_Register(lil_t lil);
int C_SND_Register(lil_t lil);
int C_HIC_Register(lil_t lil);
int C_CRT_Register(lil_t lil);

void _CPU_ErrorHandler(lil_t lil, size_t pos, const char* msg)
{
    char buffer[17];
    TXT_Reset();
    VID_Clear(COLOR_PURPLE);
    sprintf(buffer, "  Dead Run %d", pos);
    buffer[17] = 0;
    TXT_Print(buffer);
    if (strlen(buffer) < 16)
        TXT_Print("\n");
    TXT_Print("----------------");
    TXT_Print(msg);
    TXT_SetCursor(0,8);
    TXT_Print("----------------  Press Escape  ");
    cpu_lock = true;
}

int C_CPU_Register(lil_t lil)
{
	//TODO - Default commands

	//Callbacks
	lil_callback(lil, LIL_CALLBACK_ERROR,(lil_callback_proc_t)_CPU_ErrorHandler);
	return true;
}

int CPU_Init()
{
	lil = lil_new();
	int good = C_CPU_Register(lil);
	good = good & C_VID_Register(lil);
	good = good & C_TXT_Register(lil);
	good = good & C_SND_Register(lil);
	good = good & C_HIC_Register(lil);
	good = good & C_CRT_Register(lil);
	return good;
}

int CPU_Parse(char *code)
{
	//TODO - Actually do the code parsing thing
	trace("Code Parsing.");
	//trace(code);
	lil_parse(lil, code, 0, 1);
	return true;
}

int CPU_Loop()
{
	//Run the Loop function.
	if (cpu_lock)
        return true;
	lil_call(lil, "loop", 0, NULL);
	return true;
}

void CPU_Quit()
{
	lil_free(lil);
}
