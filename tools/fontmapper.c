
#include "SDL2/SDL.h"
#include <stdio.h>

#define WIDTH	4
#define HEIGHT	6
#define CHARS	128
#define CHPLN	32
#define LINES	4

int main(int argc, char *argv)
{
	SDL_Surface *font;
	unsigned long chars[CHARS];
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
		return 1;
	}
	
	font = SDL_LoadBMP("font.bmp");
	if ( font == NULL )
	{
		printf( "font.bmp is broken or missing. SDL_Error: %s\n", SDL_GetError());
		return 2;
	}
	
	for(int i = 0; i < CHARS; ++i)
	{
		chars[i] = 0;
	}
	
	int byteStep = font->format->BytesPerPixel;
	int charStep = byteStep * WIDTH;
	char *curr = (char *)(font->pixels);
	int pb = 0;
	printf("// Generated Font Map\n// For use in GTC\n\n");
	for(int set = 0; set < LINES; ++set)
	{
		for(int y = 0; y < HEIGHT; ++y)
		{
			printf("// ");
			for(int i = 0; i < CHPLN; ++i)
			{
				for(int x = 0; x < WIDTH; ++x)
				{
					int on = 0;
					for(int p = 0; p < byteStep; ++p)
					{
						//printf("%d ", curr[pb]);
						if (curr[pb])
							on = 1;
						pb++;
					}
					printf("%d", on);
					int b = (set*CHPLN) + i;
					chars[b] = (chars[b] << 1) + on;
				}
			}
			printf("\n");
		}
	}
	printf("\n");
	printf("int *font = {");
	for(int i = 0; i < CHARS - 1; ++i)
	{
		printf("%d, ", chars[i]);
	}
	printf("%d};\n",chars[CHARS-1]);
	
	SDL_FreeSurface(font);
	SDL_Quit();
}