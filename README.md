# GTC Micro-8 #
## aka. GlitchD1's Tiny Computer ##
The GTC Micro-8 was originally designed for #LOWRESJAM2016 as a method for the developer to keep within the spirit of the competition. Naturally, over time the scope of this project expanded until it had become a miniature virtual computer system.

## Requirements ##
The Micro-8 requires [SDL2](https://www.libsdl.org/) and [Premake](https://premake.github.io/) to be built on a system.
It also uses [LIL](http://runtimeterror.com/tech/lil/), [BlipBuffer](http://www.slack.net/~ant/libs/audio.html#Blip_Buffer), and [LodePNG](http://lodev.org/lodepng/). However, these are already included in the source.

## Architecture ##
### Specifications ###
As the Micro-8 has no current physical representation, the specifications may change during development.

* Display:	64x64 pixel, 16 color display using a variant of the RGBi interface.
* Video:	2k Video RAM Block with direct pixel addressing, and sync gating.
* Audio:	4 channel sound chip with square, triangle, sawtooth and Noise generation. Capable of 64 different tones in each of the 4 types.
* Storage:	8k Image Cartridge
* Input:	Keyboard (8-bit ASCII compatible)
* CPU:		Standard LIL-based Processing Unit.
* Comms:	"Serial" Port (Not yet implemented)

### Cartridge Layout ###
The Micro-8 uses an image cartridge system with 64x64 RGBA-format pixel images representing cartridges. Each cartridge currently stores 8KB of data by etching it into the 4 least significant bits of each colour value. (including alpha)

The Micro-8 cartridge data layout provides the following:

* 2048 bytes for 64 8x8 16-color sprites. Sprites with an IDs of 0-15 are normally used with maps, and IDs 60-63 are used for the icon.
* 512 bytes for a 32x32 map of sprite ID. These are normally used for backgrounds.
* 1024 bytes for the music notation bank. This stores 1024 notes for music generation.
* 4096 bytes for the game code
* 17 bytes for storing the cartridge's title
* 480 bytes for user data. This is normally used for storing save games and high score tables.
* 15 bytes for the user data checksum. This ensures that the save data can be checked for corruption.

### Command Line Parameters ###
* -sc - Save Cartridge as a png image file.
* -dc - Force loading the default, built-in cartridge.
* *.png - Load Cartridge image.
* *.muc - Load Micro-8 Uncompiled Cartridge Data.

### LIL Commands ###
The Micro-8 currently uses LIL for game code. Some unnecessary commands were removed to prevent security issues. The following are additional commands provided by the Micro-8:

* print	... - Prints everything following it to the screen. If empty just prints a new line.
* println ... - Same as print but does a new line after it.
* printat x y t - Prints t at location x,y. Doesn't wrap like normal text. Ignores New Lines.
* setcur x y - Sets the print cursor to character location x,y.
* cls c - clears screen to color c, or black if no c is provided.
* static - Draws Random static across the screen.
* dot x y - Draws a dot at location x,y.
* vol c v - Sets the volume of channel c to v.
* play c s e t - Sets channel c to play from the music sheet, starting at s, ending at t, with a delay between notes of t.
* note c n - Plays the provided note n on a channel c.
* key k - Returns True if key k is pressed.
* lastkey - Returns the identifier of the last key pressed.
* draw x y i - Draw Sprite i at location x,y.
* map x y mx my mw mh - Draw Map Data starting at location x,y on screen, and mx,my in the map. Size of mw, mh. If mh is omitted, mw will be used. If both size parameters are missing, a default of 8 is used.
* asm i s - Assigns sprite s to map id i.
* gsm i - Gets sprite assigned to map id i.
* check - Returns true if Cart Save Data is correct.
* sync - Writes changes to Cart out to cart image. Do this after making changes to cart save data.
* getc i - Get byte of cart data at ID i
* geti i - Get integer from cart data starting at ID i. Integers stored require 4 bytes of cart data.
* gets i l - Get String from Cart data, starting at ID i, until either string ends or length l.
* putc i c - Set byte of cart data at ID i to c
* puti i v - Store the value of the integer v in cart data starting at ID i. Integers stored require 4 bytes of cart data.
* puts i l s - Store the string in cart data, starting at ID i, until either string ends or length l.
* line x1 y1 x2 y2 - Draw a line on screen from position x1,y1 to x2,y2
* hline x1 y x2 - Draw a horizontal line on screen from position x1,y to x2,y
* vline x y1 y2 - Draw a vertical line on screen from position x,y1 to x,y2
* rect x y w h - Draw a hollow rectangle starting at x,y with a size of w,h
* frect x y w h - Draw a filled rectangle starting at x,y with a size of w,h
* circ x y w h - Draw a hollow ellipse starting at x,y with a size of w,h
* fcirc x y w h - Draw a filled ellipse starting at x,y with a size of w,h

### Color Reference ###
The Micro-8 uses a variant of the RGBi standard. Each pixel is actually 4 bits large and are stored in VRAM as pairs. The red, green and blue colors (RGB.) are either on or off, with 2 levels of intensity as set by the intensity bit. (...i)
A pixel with a color value of 0 is considered transparent.

* 0 - Transparent
* 1 - Black
* 2 - Navy
* 3 - Blue
* 4 - Green
* 5 - Lime
* 6 - Cyan
* 7 - Aqua
* 8 - Maroon
* 9 - Red
* 10/A - Purple
* 11/B - Pink
* 12/C - Olive
* 13/D - Yellow
* 14/E - Gray
* 15/F - White

## TODO List ##
* Serial Link
* Add Sinclair-style Joystick support. 1-5 for Port 1 and 6-0 for Port 2.
* Writing to Standard Out (for use with Utility carts mainly.)
* Make some Utility carts to assist in making games
* Develop a better built-in cartridge
* Make a more interesting start-up sequence
* Tidy up the coding reference into a single document.
* Extend the Keyboard Input system to include more keys
* Code Review and Refactor
* Remake heaps of old games for demonstration purposes

### Retro Remakes List ###
The Idea of this list is that I may come up with new feature and/or find bugs while developing clone versions of these games. This also allows me to provide examples and demos for how to create new games for the system.

* Space Invaders (In Development)
* Missile Command
* Asteroids
* Battle Tanks
* Space War
* Pac-man
* Berzerk
* Frogger
* Lunar Lander
* Pengo
* 1942
* Rocket Racer
* Pitfall
* Dig Dug
* Super Luigi
* Pocket Monsters

### Utilities List ###

* Music Recorder Player
* Graphics Editor
* "BASIC/LIL" Interpreter

## Game Carts ##
To use the following game carts, simply save them to your computer and open them with the Micro8 by drag'n'dropping them onto the executable.

![ping.png](https://bitbucket.org/repo/aBn7Gz/images/3612137139-ping.png)
![breakanoid.png](https://bitbucket.org/repo/aBn7Gz/images/1508394852-breakanoid.png)

The source files for these cartridges can be found in the cart_src/ folder of the repository.