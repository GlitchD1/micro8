#!lua

-- A solution contains projects, and defines the available configurations
solution "Micro8"
   configurations { "Debug", "Release" }

   -- A project defines one build target
   project "Micro8"
      kind "ConsoleApp"
      language "C"
      files { "src/**.h", "src/**.c"}
      buildoptions "-std=c11"
      links { "m", "SDL2" };
      includedirs { "/usr/include/SDL2/" }

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }
